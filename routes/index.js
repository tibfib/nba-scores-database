const router = require('express').Router();
const axios = require('axios');
const path = require('path');

const adapt = require('../lib/adapt-data');
const teams = require('../lib/team-mappings');

const { addData, getLatestGame } = require('../lib/storage');

function getDate() {
    let d = new Date();
    return `${d.getMonth() + 1}/${d.getDate()}/${d.getFullYear()}`;
}

function reload() {
    console.log('reloading'); //eslint-disable-line
    return axios
        .get('http://stats.nba.com/stats/scoreboard/', {
            params: {
                LeagueID: '00',
                GameDate: getDate(),
                DayOffset: 0
            }
        })
        .then(({ data }) => {
            const { games, scores } = adapt(data);
            addData(games, scores);
        });
}

router.get('/reload', function(req, res) {
    reload()
        .then(() => res.json(true))
        .catch(er => {
            res.json(false);
            console.error('got error', er); // eslint-disable-line
        });
});

router.get('/download.json', function(req, res) {
    res.sendFile(path.resolve(__dirname, '../data/data.json'));
});

const INTERVAL_TIME = 90000; // 90 sec
setInterval(
    //prettier-ignore
    () => reload().then(a => a).catch(e => e),
    INTERVAL_TIME
);

router.get('/query', function(req, res) {
    getLatestGame(teams.get((req.query.team || '').toLowerCase()), game => {
        if (game) {
            res.json(game);
        } else {
            res.json(false);
        }
    });
});

router.post('/query', function(req, res) {
    const { team, endpoint } = req.body;

    console.log('got body', req.body);

    getLatestGame(teams.get((team || '').toLowerCase()), game => {
        axios.post(endpoint, game).catch(er => {
            console.error('error', er);
        });
    });

    res.end();
});

module.exports = router;
