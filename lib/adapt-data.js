const _ = require('lodash');

function adapt(data) {
    let [gameHeader, lineScore] = data.resultSets;
    let games = _.keyBy(
        gameHeader.rowSet.map(gameValues => _.zipObject(gameHeader.headers, gameValues)),
        'GAME_ID'
    );
    let scores = lineScore.rowSet.map(scoreValues => _.zipObject(lineScore.headers, scoreValues));

    return { games, scores };
}

module.exports = adapt;
