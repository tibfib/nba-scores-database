const AtlantaHawks = 1610612737;
const BostonCeltics = 1610612738;
const BrooklynNets = 1610612751;
const CharlotteHornets = 1610612766;
const ChicagoBulls = 1610612741;
const ClevelandCavaliers = 1610612739;
const DallasMavericks = 1610612742;
const DenverNuggets = 1610612743;
const DetroitPistons = 1610612765;
const GoldenStateWarriors = 1610612744;
const HoustonRockets = 1610612745;
const IndianaPacers = 1610612754;
const LosAngelesClippers = 1610612746;
const LosAngelesLakers = 1610612747;
const MemphisGrizzlies = 1610612763;
const MiamiHeat = 1610612748;
const MilwaukeeBucks = 1610612749;
const MinnesotaTimberwolves = 1610612750;
const NewOrleansPelicans = 1610612740;
const NewYorkKnicks = 1610612752;
const OklahomaCityThunder = 1610612760;
const OrlandoMagic = 1610612753;
const Philadelphia76ers = 1610612755;
const PhoenixSuns = 1610612756;
const PortlandTrailBlazers = 1610612757;
const SacramentoKings = 1610612758;
const SanAntonioSpurs = 1610612759;
const TorontoRaptors = 1610612761;
const UtahJazz = 1610612762;
const WashingtonWizards = 1610612764;

let keys = [
    { terms: ['atlanta', 'hawks', 'atl'], team: AtlantaHawks },
    { terms: ['boston', 'celtics', 'bos'], team: BostonCeltics },
    { terms: ['brooklyn', 'nets', 'brk'], team: BrooklynNets },
    { terms: ['charlotte', 'hornets', 'cha'], team: CharlotteHornets },
    { terms: ['chicago', 'bulls', 'chc'], team: ChicagoBulls },
    { terms: ['cleveland', 'cavaliers', 'cavs', 'cav', 'clv', 'cle'], team: ClevelandCavaliers },
    { terms: ['dallas', 'mavericks', 'dal', 'mavs'], team: DallasMavericks },
    { terms: ['denver', 'nuggets', 'den'], team: DenverNuggets },
    { terms: ['detroit', 'pistons', 'drt'], team: DetroitPistons },
    {
        terms: ['golden state', 'golden', 'state', 'warriors', 'gsw', 'dubs'],
        team: GoldenStateWarriors
    },
    { terms: ['houston rockets', 'houston', 'rockets', 'hou'], team: HoustonRockets },
    { terms: ['indiana pacers', 'indiana', 'pacers', 'ind'], team: IndianaPacers },
    { terms: ['los angeles clippers', 'clippers', 'clp'], team: LosAngelesClippers },
    { terms: ['los angeles lakers', 'lakers', 'lak'], team: LosAngelesLakers },
    { terms: ['memphis grizzlies', 'memphis', 'grizzlies', 'mem'], team: MemphisGrizzlies },
    { terms: ['miami heat', 'miami', 'heat', 'mia'], team: MiamiHeat },
    { terms: ['milwaukee bucks', 'milwaukee', 'bucks', 'mil'], team: MilwaukeeBucks },
    {
        terms: ['minnesota timberwolves', 'minnesota', 'timberwolves', 'min'],
        team: MinnesotaTimberwolves
    },
    {
        terms: ['new orleans pelicans', 'new orleans', 'orleans', 'no', 'pelicans', 'nop', 'pels'],
        team: NewOrleansPelicans
    },
    { terms: ['new york knicks', 'new york', 'york', 'ny', 'knicks', 'nyk'], team: NewYorkKnicks },
    {
        terms: ['oklahoma city thunder', 'oklahoma city', 'oklahoma', 'thunder', 'okc'],
        team: OklahomaCityThunder
    },
    { terms: ['orlando magic', 'orlando', 'magic', 'orl'], team: OrlandoMagic },
    {
        terms: ['philadelphia 76ers', 'philadelphia', '76ers', '76', 'sixer', 'sixers', 'phi'],
        team: Philadelphia76ers
    },
    { terms: ['phoenix suns', 'phoenix', 'suns', 'phx'], team: PhoenixSuns },
    {
        terms: ['portland trailblazers', 'portland', 'trailblazers', 'prd'],
        team: PortlandTrailBlazers
    },
    { terms: ['sacramento kings', 'sacramento', 'kings', 'sac'], team: SacramentoKings },
    { terms: ['san antonio spurs', 'san antonio', 'spurs', 'sas'], team: SanAntonioSpurs },
    { terms: ['toronto raptors', 'toronto', 'raptors', 'tor'], team: TorontoRaptors },
    { terms: ['utah jazz', 'utah', 'jazz', 'uta'], team: UtahJazz },
    { terms: ['washington wizards', 'washington', 'wizards', 'was'], team: WashingtonWizards }
];

const map = new Map();
keys.forEach(({ terms, team }) => {
    terms.forEach(term => {
        map.set(term, team);
    });
});
module.exports = map;
