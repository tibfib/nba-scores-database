const fs = require('fs');
const path = require('path');
const _ = require('lodash');

function saveData(json) {
    fs.writeFile(
        path.resolve(__dirname, '../data/data.json'),
        JSON.stringify(json),
        'utf8',
        error => error
    );
}
function readData(callback) {
    fs.readFile(path.resolve(__dirname, '../data/data.json'), 'utf8', function readFileCallback(
        err,
        data
    ) {
        if (err) {
            console.error(err); // eslint-disable-line
            callback({ games: {}, team_games: {} });
        } else {
            let obj = JSON.parse(data); //now it an object
            callback(obj);
        }
    });
}

function addData(newGames, newScores) {
    readData(data => {
        let games = { ...data.games, ...newGames };
        let team_games = { ...data.team_games };

        newScores.forEach(teamScore => {
            let game = { ...games[teamScore.GAME_ID] };
            if (game.HOME_TEAM_ID === teamScore.TEAM_ID) game.HOME = teamScore;
            else game.AWAY = teamScore;

            games[game.GAME_ID] = game;

            let currentGames = data.team_games[teamScore.TEAM_ID] || [];
            if (!currentGames.includes(teamScore.GAME_ID)) {
                team_games[teamScore.TEAM_ID] = [...currentGames, teamScore.GAME_ID];
            }
        });

        saveData({ games, team_games, updated: new Date().toLocaleString() });
    });
}

function getGames(callback) {
    return readData(callback);
}

function getLatestGame(teamId, callback) {
    readData(data => {
        let gameId = _.last(_.get(data, ['team_games', teamId]));
        callback(_.get(data, ['games', gameId]));
    });
}

module.exports = { addData, getGames, getLatestGame };
